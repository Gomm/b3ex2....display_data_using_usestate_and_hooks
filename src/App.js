import React, { useState } from 'react';
import './App.css';


const App = () => {

  const [data, setData] = useState('');


  const handleChange = e => {
      setData (e.target.value);
    console.log(data);
  };

  const nodata = 'no data provided';

  return (
    <div>
      <input onChange={handleChange} />
      <h1>{data || nodata} </h1>
    </div>
  );
};

export default App;



